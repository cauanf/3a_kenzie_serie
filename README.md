# Rota **/series** método **POST**

## Entrada e saída **ROTA /series método POST.**
<img width="100%" src='./imgs/requisicao_post.png' alt='exemplo requisição post'/>

</br>
</br>

# Rota **/series** método **GET**

## Entrada e saída **ROTA /series método GET** com a **TABELA criada.**
<img width="100%" src='./imgs/requisicao_get_com_tabela.png' alt='exemplo requisição get com tabela'/>

</br>

<hr></hr>

## Entrada e saída **ROTA /series método GET** sem a **TABELA criada** ou **SEM DADOS na TABELA.**
<img width="100%" src='./imgs/requisicao_get_sem_tabela.png' alt='exemplo requisição get sem tabela'/>

</br>
</br>

# Rota **/series** método **GET**

## Entrada e saída **ROTA /series/\<int:serie_id> método GET** com a **TABELA criada.**
<img width="100%" src='./imgs/requisicao_get_params_com_tabela.png' alt='exemplo requisição get com tabela'/>

</br>

<hr></hr>

## Entrada e saída **ROTA /series/\<int:serie_id> método GET** sem a **TABELA criada** ou **SEM DADOS na TABELA.**
<img width="100%" src='./imgs/requisicao_get_params_sem_tabela.png' alt='exemplo requisição get sem tabela'/>

</br>